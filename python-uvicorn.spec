%global pkgname uvicorn

Name:           python-%{pkgname}
Version:        0.34.0
Release:        1
Summary:        The lightning-fast ASGI server
License:        BSD
URL:            https://www.uvicorn.org
Source0:	https://files.pythonhosted.org/packages/4b/4d/938bd85e5bf2edeec766267a5015ad969730bb91e31b44021dfe8b22df6c/uvicorn-0.34.0.tar.gz
BuildArch:      noarch
 
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-hatchling
BuildRequires:  python3-httptools
BuildRequires:  python3-dotenv
BuildRequires:  python3-PyYAML
BuildRequires:  python3-uvloop
BuildRequires:  python3-websockets

%description 
Uvicorn is an ASGI web server implementation for Python.
Until recently Python has lacked a minimal low-level server/application interface for async frameworks.
The ASGI specification fills this gap, and means we're now able to start building a common set of tooling 
usable across all async frameworks.
Uvicorn supports HTTP/1.1 and WebSockets.
 
%package -n python3-%{pkgname}
Summary:        %{summary}
 
%description -n python3-%{pkgname} 
Uvicorn is an ASGI web server implementation for Python.
Until recently Python has lacked a minimal low-level server/application interface for async frameworks.
The ASGI specification fills this gap, and means we're now able to start building a common set of tooling
usable across all async frameworks.
Uvicorn supports HTTP/1.1 and WebSockets.
 
%prep
%autosetup -n uvicorn-%{version} -p 1
 
%build
%pyproject_build
 
%install
%pyproject_install
 
%files -n python3-%{pkgname}
%license LICENSE.md
%doc README.md
%{_bindir}/uvicorn
%{python3_sitelib}/uvicorn*.dist-info/
%{python3_sitelib}/uvicorn

%changelog
* Mon Dec 16 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 0.34.0-1
- Update package to version 0.34.0
  Add content-length to 500 response in wsproto implementation
  Drop support for Python 3.8
  Remove WatchGod support for --reload

* Thu Oct 24 2024 shaoning <shaoning@kylinos.cn> - 0.32.0-1
- Update package to version 0.32.0
    * Add reason support on WebSocketDisconnectEvent
    * Add pragma: full coverage to Process.is_alive
    * Remove signal testing order dependency
    * Improve ProxyHeadersMiddleware
    * Add support for [*] in trusted hosts

* Fri Jun 28 2024 liyue01 <liyue01@kylinos.cn> - 0.30.1-1
- Update package to version 0.30.1
- Fixed ：Allow horizontal tabs in response header values
- Added New multiprocess manager and Allow ConfigParser or a io.IO[Any] on log_config
- Fixed：Suppress side-effects of signal propagation ，Send content-length header on 5xx
- Added Cooperative signal handling
- Fixed ：Revert raise ClientDisconnected on HTTP
- Added Raise ClientDisconnected on send() when client disconnected

* Mon Feb 26 2024 wangjunqi <wangjunqi@kylinos.cn> - 0.27.1-1
- Update package to version 0.27.1

* Tue Jul 25 2023 ysliu <ysliuci@isoftstone.com> - 0.23.1-1
- Initial package.
